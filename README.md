# README

## What is this repository for?

* Shows an example of WebAssembly - compiled c to wasm
* v 1.0

## How do I get set up?

### Compile Emscripten from source

	$ git clone https://github.com/juj/emsdk.git
	$ cd emsdk
	$ ./emsdk install sdk-incoming-64bit binaryen-master-64bit
	$ ./emsdk activate sdk-incoming-64bit binaryen-master-64bit

**Note:** This can take quite long time.

Then use this comand to add relevant PATH's: 

	$ source ./emsdk_env.sh

For more information refer to: [WebAssembly Getting started Guide](http://webassembly.org/getting-started/developers-guide/)

### Compile c into wasm

After the path's are added to your current enviroment, compile the c file into wasm binary:

    emcc fibonacci.c -Os -s WASM=1 -s SIDE_MODULE=1 -o fibonacci.wasm
    
This will be compiled as Side Module, therefore no need to add Emscripten KEEPALIVE.
Afterwards, you need to load this file and compile it on the client side. This can be achieved like this:

```
:::js
fetch('fibonacci.wasm').then(response => {
  return response.arrayBuffer();
}).then(bytes => {
  return WebAssembly.compile(bytes);
}).then(wasm => {
    const imports = {
      env: {
        memoryBase: 0,
        tableBase: 0,
        memory: new WebAssembly.Memory({
          initial: 256
        }),
        table: new WebAssembly.Table({
          initial: 0,
          element: 'anyfunc'
        })
      }
    };
    return WebAssembly.instantiate(wasm, imports);
  }
).then(results => {
  // results is instantiated module, do something with it
  // You can call results.exports._myfunction() where myfunction is name of your function.
  // The underscore _ is necessary!
});
```
Now you should be up and running. If you want to make your life a bit easier, keep reading...

### Compile c into wasm + js
Instead of compiling the binary on client side, you can let the emscripten do it for you.
It will generate all the necessary javascript glue code, so you don't need to take care of this.

    emcc fibonacci.c -O3 -s WASM=1 -o fibonacci.js
  
Then just include the script in your html and you are ready to go.

Note that unused functions (e.g. not called from c code) will be removed. In order to stop this behaviour, you must use **EMSCRIPTEN_KEEPALIVE** for each function. 
Have a look at [Emscripten Documentation](https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html#interacting-with-an-api-written-in-c-c-from-nodejs) for more.

### Start server

Last thing you need to do is serve the files over an HTTP server(because cross-origin request are forbidden in chrome and opera). You can use emscripten server like this:

    emrun --no_browser --no_emrun_detect --port 8080 .

Or use anything you like. That's up to you.
