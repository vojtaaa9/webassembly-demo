$(document).ready(function(){
  var textarea = "";
  var file = 'fibonacci.wasm';
  var time;
  var module;

  fetch(file).then(response => {
    printLine('Fetching fibonacci.wasm file.');
    time = Date.now();
    return response.arrayBuffer();
  }).then(bytes => {
    printLine('Fetching took ' + (Date.now() - time) + ' ms.');
    printLine('Compiling ' + file + '...');
    time = Date.now();
    return WebAssembly.compile(bytes);
  }).then(wasm => {
      const imports = {
        env: {
          memoryBase: 0,
          tableBase: 0,
          memory: new WebAssembly.Memory({
            initial: 256
          }),
          table: new WebAssembly.Table({
            initial: 0,
            element: 'anyfunc'
          })
        }
      };
      return WebAssembly.instantiate(wasm, imports);
    }
  ).then(results => {
    printLine(file + ' succesfully compiled. It took ' + (Date.now() - time) + ' ms.');
    printLine('');
    module = results;
  });

  $("#fibonacci_go").click(function(){
    var max = $('#fibonacci_max').val();

    var suite = new Benchmark.Suite;

    printLine("Benchmark Started.");
    printLine("Fibonacci(" + max + ")");
    // add tests
    suite.add('C loop      ', function() {
      module.exports._fib(max);
    })
    .add('C recursive ', function() {
      module.exports._fibonacciRec(max);
    })
    .add('C memo      ', function() {
      module.exports._fibonacciMemo(max);
    })
    .add('JS loop     ', function() {
      fiboJs(max);
    })
    .add('JS recursive', function() {
      fiboJsRec(max);
    })
    .add('JS memo     ', function() {
      fiboJsMemo(max);
    })
    .on('cycle', function(event) {
      printLine(String(event.target));
    })
    .on('complete', function() {
      printLine("---------------\n");
      printLine("C loop is " + (this[0].hz/this[3].hz).toFixed(2) + "x than JS loop");
      printLine("C recursive is " + (this[1].hz/this[4].hz).toFixed(2) + "x than JS recursive");
      printLine("C memo is " + (this[2].hz/this[5].hz).toFixed(2) + "x than JS memo");
      printLine('');

      printLine('Fastest is ' + this.filter('fastest').map('name'));
      printLine('Slowest is ' + this.filter('slowest').map('name'));
      printLine("Benchmark finished.");
      printLine("---------------\n");
    })
    // run async
    .run({ 'async': true });
  });

  function printLine(text) {
    textarea += text + '\n';
    $('#output').val(textarea);
  }

  function fiboJs(num){
    var a = 1, b = 0, temp;

    while (num >= 0){
      temp = a;
      a = a + b;
      b = temp;
      num--;
    }

    return b;
  }

  const fiboJsRec = (num) => {
    if (num <= 1) return 1;

    return fiboJsRec(num - 1) + fiboJsRec(num - 2);
  }

  const fiboJsMemo = (num, memo) => {
    memo = memo || {};

    if (memo[num]) return memo[num];
    if (num <= 1) return 1;

    return memo[num] = fiboJsMemo(num - 1, memo) + fiboJsMemo(num - 2, memo);
  }
});
